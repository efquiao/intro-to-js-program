let videoElem = document.getElementById("vision-video");
let playButton = document.getElementById("play-video-btn");


playButton.addEventListener("click", handlePlayButton);

function handlePlayButton(){

  if (videoElem.paused){
     videoElem.play();
  playButton.innerHTML = 'PAUSE';

  }else{
    videoElem.pause();
    playButton.innerHTML = 'PLAY'
  }
 }
  

